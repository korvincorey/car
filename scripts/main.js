/**
 * For starting - create car with consumption:
 * var car = Car(5);
 *
 * Car module. Created car with custom consumption.
 * @param consumption {number}
 * @return {{
 *            start: start,
*             riding: riding,
 *            checkGas: checkGas,
  *           refueling: refueling
*           }}
 * @constructor
 */
function Car(consumption) {
  if (!isNumeric(consumption)) {
    showMessage('Wrong consumption', 'error');
    return;
  }

  let gasBalance = 100;
  let gasConsumption = consumption / 100;
  let gasResidue;
  let gasVolume = 200;
  let ignition = false;
  let ready = false;

  /**
	 * Check gas amount after riding.
	 * @param distance {number} - Riding distance.
	 * @return {number} - Gas amount.
	 */
  function gasCheck(distance) {
    if (gasBalance <= 0) {
      return 0;
    }

    let gasForRide = Math.round(distance * gasConsumption);

    return gasBalance - gasForRide;
  }

  /**
	 * Show message for a user in the console.
	 * @param message {string} - Text message for user.
	 * @param type {string} - Type of the message (error, warning, log). log by default.
	 */
  function showMessage(message, type) {
    let messageText = message ? message : 'Error in program. Please call - 066 083 07 06';

    switch (type) {
      case 'error':
        console.error(messageText);
        break;
      case 'warning':
        console.warn(messageText);
        break;
      case 'log':
        console.log(messageText);
        break;
      default:
        console.log(messageText);
        break;
    }
  }

  /**
	 * Check car for ride.
	 * @param distance {number} - Ride distance.
	 */
  function checkRide(distance) {
    gasResidue = gasCheck(distance);
  }

  /**
	 * Check value to number.
	 * @param n {void} - value.
	 * @return {boolean} - Is number.
	 */
  function isNumeric(n) {
    return !isNaN(parseFloat(n)) && isFinite(n);
  }

  /**
	 * Public methods object.
	 */
  return {
    /**
		 * Start car.
		 */
    start: function() {
      ignition = true;

      if (gasBalance <= 0) {
        showMessage('You don\'t have gas. Please refuel the car.', 'error');
        ready = false;
        return;
      }

      ready = true;

      showMessage('Ingition', 'log');
    },

    /**
		 * Riding function.
		 * @param distance {number} - Riding distance.
		 */
    riding: function(distance) {
      if (!isNumeric(distance)) {
        showMessage('Wrong distance', 'error');
        return;
      }

      if (!ignition) {
        showMessage('You need start car', 'error');
        return;
      }

      if (!ready) {
        ignition = false;
        showMessage('You need gas station', 'error');
        return;
      }

      checkRide(distance);

      let gasDriven = Math.round(gasBalance * gasConsumption * 100);

      if (gasResidue <= 0) {
        let distanceLeft = Math.round(distance - gasDriven);

        gasBalance = 0;
        let neddedGas = distanceLeft * gasConsumption;

        showMessage('Gas over. You have driven - ' + gasDriven + '. You need ' + + neddedGas + ' liters. ' + distanceLeft + 'km left', 'warning');
      }

      if (gasResidue > 0) {
        gasBalance = gasResidue;
        showMessage('You arrived. Gas balance - ' + gasResidue, 'log');
      }
    },

    /**
		 * Check gas function.
		 */
    checkGas: function() {
      showMessage('Gas - ' + gasBalance, 'log');
    },

    /**
		 * Refueling function.
		 * @param gas
		 */
    refueling: function(gas) {
      if (!isNumeric(gas)) {
        showMessage('Wrong gas amount', 'error');
        return;
      }

      if (gasVolume === gasBalance) {
        showMessage('Gasoline tank is full', 'warning');
      } else if (gasVolume < gasBalance + gas) {
        gasBalance = 200;
        let excess = Math.round(gas - gasBalance);
        showMessage('Gasoline tank is full. Excess - ' + excess, 'log');
      } else {
        gasBalance = Math.round(gasBalance + gas);
        showMessage('Gas balance - ' + gasBalance, 'log');
      }
    },
  };
}
